const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const UserSchema = new Schema({
	_id:{
		type: ObjectId,
		index: true,
		required: true,
		auto: true,
	},
	name: {
		type: String,
		trim: true,
		required: true,
	},
	username: {
		type: String,
		trim: true,
		required: true,
	},
	email: {
		type: String,
		trim: true,
		required: true
	},
	password: {
		type: String,
		trim: true,
		required: true
	},
	created_at: { 
		type: Date, 
		default: Date.now 
	}
});

module.exports = mongoose.model('User', UserSchema);