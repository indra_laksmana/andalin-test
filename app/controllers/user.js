const userModel = require('../models/user');
const argon2i = require('argon2-ffi').argon2i;
const crypto = require('crypto');
const Promise = require('bluebird');
const randomBytes = Promise.promisify(crypto.randomBytes);

module.exports = {
	create: function (req, res, next) {
		var password = new Buffer(req.body.password);
		var options = { timeCost: 2, memoryCost: 1 << 10, parallelism: 2, hashLength: 16 };

		userModel.findOne({ email: req.body.email }, function (err, user) {
			if (err) {
				res.status(500).json({ error: err });
				next(err);
			} else {
				if(user){
					res.status(200).json({ error: 'Email has already been taken' });
				}else{
					randomBytes(32).then(salt => argon2i.hash(password, salt, options))
					.then((encoded) => {
						userModel.create({
							name: req.body.name,
							username: req.body.username,
							email: req.body.email,
							password: encoded
						}, function (err, userInfo) {
							if (err){
								next(err);
							}
							else{
								return res.status(200).json({
									data: {
										user: {
											email: userInfo.email,
											name: userInfo.name,
											username: userInfo.username,
											_id: userInfo._id
										}
									}
								});
							}
						});
					});
				}
			} 
		});
	},
	update: function (req, res, next) {
		userModel.findOne({ _id: req.params.id }, function (err, user) {
			if (err) {
				res.status(500).json({ error: err });
				next(err);
			} else {
				user.name = req.body.name;
				user.username = req.body.username;
				user.email = req.body.email;
				user.save();
				return res.status(200).json({
					data: {
						success: 'Update has been successfully' 
					}
				});
			}
		});

	},
	read: function (req, res, next) {
		userModel.findById(req.params.id, function (err, userInfo) {
			if (err) {
				res.status(404).json({ error: 'User not found' })
				next();
			} else {
				res.status(200).json({ data: { user: userInfo } });
			}
		});
	},
	delete: function (req, res, next) {
		userModel.findById(req.params.id, function (err, user) {
			if (err) {
				res.status(404).json({ error: 'User not found' })
				next();
			} else {
				user.remove();
				return res.status(200).json({
					data: {
						success: 'User has been removed'
					}
				});
			}
		});
	},
}