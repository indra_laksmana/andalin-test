const express = require('express');
const logger = require('morgan');
const user = require('./routes/user');
const bodyParser = require('body-parser');
const mongoose = require('./config/database'); //database configuration
const config = require('./config/app');
const middleware = require('./app/middleware');
const cors = require('cors')
const app = express();
const jwt = require('jsonwebtoken');

app.use(cors());

app.set('secretKey', config.secret); // jwt secret token

// connection to mongodb
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/api/v1/', function (req, res) {
	var token = jwt.sign({ id: 1 }, config.secret, { expiresIn: '1d' });
	res.json({ "USER-NODE-API": "Build REST API with node.js", token: token });
});

// private route
app.use('/api/v1/user', middleware.checkToken, user);

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function (req, res, next) {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// handle errors
app.use(function (err, req, res, next) {
	console.log(err);

	if (err.status === 404)
		res.status(404).json({ message: "Not found" });
	else
		res.status(500).json({ message: "Something looks wrong!!!" });
});

app.listen(4000, function () { console.log('Node server listening on port 4000'); });