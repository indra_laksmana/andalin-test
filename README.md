# Node API With JWT

## Getting Started
This app uses **node.js**, **mongodb**, **docker-ce**, **docker-compose**. So, please install all requirement on your computer system before build this app.

## How to build app?
Just run below command to build app.

```
docker-compose up --build
```


