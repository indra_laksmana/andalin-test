const express = require('express');
const router = express.Router();
const multer = require('multer');
const formData = multer();

const userController = require('../app/controllers/user');

router.post('/create', formData.fields([]), userController.create);
router.post('/update/:id', formData.fields([]), userController.update);
router.get('/:id', formData.fields([]), userController.read);
router.delete('/:id', formData.fields([]), userController.delete);

module.exports = router;